# README #

### What is this repository for? ###

* Repository for research projects in summer 2017
* [Documentation on Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Make sure you have git installed, e.g., try 'git help' or
    git --version

### Get started with command line - clone existing project ###

    git clone https://bitbucket.org/mikus_bucket/test_repo.git <dir_where_you_want_to_clone_to>


Your local git repository consists of the instances

* working directory (the actual files)

* index  

* head

### Add, Commit, and Push Files... ###

To upload something you need to

a) stage your changes to the index (prepare commit)

    git add <file>

b) commit the changes to your local repository

    git commit -m "your message"

c) push your changes to a branch of your choice at the remote repository

    git push <remote>

In step c) the remote repository is also referred to as 'origin', because that's where
you cloned it from. Therefore, in this setting the command for step c) becomes

    git push origin

That should be it! Good luck! 

-enter username/password

### Homework 0 ###

Compile the LaTeX-template given in the folder __template_clean

    author2010work.ltx

Add a citation, e.g., regarding the paper of your project, and upload the compiled .pdf as well the updated 
1Content.tex to the repository.


### Contribution guidelines ###

* Please commit timely when you have new work prepared. The earlier I have your work
the more feedback I can give you on the meetings. 

* Each source file should contain a function signature and some information on input, output, author, and date




### Not relevant: Get started with command line - add existing project ###

Step 1: Switch to your repository's directory

    cd /path/to/your/repo
Step 2: Connect your existing repository to Bitbucket

    git remote add origin https://mikus_bucket@bitbucket.org/mikus_bucket/bb101repo.git
    git push -u origin master